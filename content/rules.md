---
title: 'Normes bàsiques' # in any language you want
# url: "/archive"
layout: navigation
---

## Recordeu ! ##

* El club és un espai per compartir i passar-s'ho bé, cal respectar-se els uns als altres i no enfadar-se ni barallar-se per qualsevol situació.
* Cal tenir cura de l'escola i no entrar en espais que no ens han estat cedits per dur a terme aquesta activitat.
* Els jocs s'han de cuidar i se n'ha de fer un bon ús.
* Es recollirà i tornarà al seu lloc els jocs abans d'agafar-ne un altre.
* No es pot menjar mentre s'està jugant.
* No es poden compartir fotografies i gravacions on es vegin les cares dels altres infants.
* Els infants hauran de venir sempre acompanyats d'un adult que se'n faci responsable en tot moment.

També us hi pdeu baixar les normes amb el següent qr:

{{< figure src="../img/qr_animated.gif" alt="rules" >}}
