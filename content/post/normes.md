---
title: "Normes bàsiques"
date: 2023-10-21T20:23:13+02:00
author: "Oscar"
layout: "posts"
tags: ["normes"]
---

Com ja sabeu en el club de jocs de l'escola Poble Nou hi ha unes normes, que no varien gaire de les de l'any passat. Les teniu en el següent [enllaç]({{< ref "/rules" >}} ). També disposem del codi qr amb les normes.

{{< figure src="../../img/qr_animated.gif" alt="rules" >}}
