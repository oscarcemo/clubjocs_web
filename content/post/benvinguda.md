---
title: "Benvinguda"
date: 2023-04-26T22:49:13+02:00
author: "Oscar"
layout: "posts"
tags: ["instagram"]
---

## Benvinguts

Us donem la benvinguda al nou espai web del club jocs de taula Escola Poble Nou. A mesura que es tingui temps s'anirà mijorant la web i tindrem mes continguts.

Tenim també compte en instagram. Podeu anar directament amb el link que trobareu a sota.
😲

{{< figure src="../../img/follow-instagram-sparkle.gif" alt="instagram" link="https://www.instagram.com/clubjocsdetaula_escolapoblenou/" >}}
