---
title: "Inici del club curs 2023/2024"
date: 2023-10-21T20:03:13+02:00
author: "Oscar"
layout: "posts"
tags: ["links"]
---

## Benvinguts de nou

Ja som aquí de nou en aquest curs 2023/2024. Com l'any passat seran el dijous cd 17:40 a 19:00 hores cada dues setmanes. Aquest any disposarem d'un munt de jocs nous gràcies a les aportacions de l'[AFA de l'escola Poble Nou](https://www.afapoblenou.cat/), [Mathom Store](https://mathom.es/), les pròpies famílies organitzadores, etc.
