---
title: 'Qui som ?' # in any language you want
# url: "/archive"
layout: navigation
---

Som mares i pares que pertanyem a l'[AFA de l'escola Poble Nou](https://www.afapoblenou.cat/). Dues vegades al mes ens reunim per jugar a jocs de taula amb les nostres filles i fills i així gaudim de jocs que ens apassionen a dins de l'espai escolar.

La idea va succeir de mares i pares que tenim com afició jugar aquest tipus de jocs i vàrem comentar de poder obrir aquest tipus de joc a més alumnes de l'escola perquè entenem que és un altre tipus de joc de què normalment juguen la majoria dels alumnes. Ara mateix som aquestes famílies qui portem els nostres propis jocs perquè puguin gaudir la resta dels alumnes, però un dels nostres objectius és que el club tingui els seus propis títols per la ludoteca.

La nostra filosofia és ben fàcil: **Donar la possibilitat els alumnes de l'escola Poble Nou de descobrir aquesta manera de jugar i que ho puguin fer dins de l'escola i amb els seus companys del col·legi.**

### **Us animem a tots a vindre i passar-ho bé!** ###
