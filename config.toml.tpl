# Hey, thanks for using my theme. If you like this theme,checkout my repo (https://gitlab.com/avron/gruvhugo"
# and drop a star while you're at it. ;)
baseurl = "https://clubjocsdetaula.afapoblenou.cat"
title = "Club de jocs de taula Escola Poble Nou"
languageCode = "ca"
paginate = "10" # Number of posts per page
#disqusShortname = "" # Enable comments by entering your Disqus shortname
#googleAnalytics = "" # Enable Google Analytics by entering your tracking id
#-------------------------------------------------------------------------------
# DEFAULT SETTINGS
#-------------------------------------------------------------------------------

# Default Theme
theme= "clubjocs_theme"

# Default Post Extension
defaultExtension= "html"

# Default metadata format for newly created frontmatter using
# hugo new command. "toml", "yaml", or "json"
metaDataFormat= "yaml"

[taxonomies]
  tag      = "tags"
  category = "categories"
  series   = "series"

[params.main]
  enabled = true
  enableKatex = true
  metaKeywords = "fast, hugo, theme, minimal, gruvbox"
  headerTitle = "Club jocs de taula Escola Poble Nou"
  name = "club_jocs_de_taula_escola_poble_nou"
  img = "img/logo.webp"
  #img_title = "Thanks to Alba for the image."
  icon = "img/logo.webp"
  favicon = "img/favicon.ico"
  quote = "Volem que el nens i nenes s'ho passin bé jugant."
  description= "Us donem la benviguda a l'espai web Club jocs de taula Escola Poble Nou."
  licenseUrl = ""
  contactFormAction = "${CONTACT_URL}"
  contact_key = "${CONTACT_KEY}"
  hcaptcha_key = "${HCAPTCHA_KEY}"

 # Social icons
 # [[params.social]]
   # name = "email"
   # url  = "mailto:mail@mail.com"

  [[params.social]]
    name = "instagram"
    url  = "https://www.instagram.com/clubjocsdetaula_escolapoblenou/"

[params.style]
  # add path to custom CSS files here
  custom_css = []
  # add path to custom SCSS files here
  custom_scss = []

[menu]
  [[menu.main]]
    identifier = "contacte"
    name       = "Contacte"
    url        = "contacte"

  [[menu.main]]
    identifier = "posts"
    name       = "Posts"
    url        = "post/"

  [[menu.main]]
    identifier = "aboutus"
    name       = "Qui som ?"
    url        = "about"

  [[menu.main]]
    identifier = "rules"
    name       = "Normes"
    url        = "rules"

[markup]
  defaultMarkdownHandler = "goldmark"
  [markup.asciidocExt]
    backend = "html5"
    docType = "article"
    extensions = []
    failureLevel = "fatal"
    noHeaderOrFooter = true
    safeMode = "unsafe"
    sectionNumbers = false
    trace = false
    verbose = true
    workingFolderCurrent = false
    [markup.asciidocExt.attributes]
  [markup.blackFriday]
    angledQuotes = false
    footnoteAnchorPrefix = ""
    footnoteReturnLinkContents = ""
    fractions = true
    hrefTargetBlank = false
    latexDashes = true
    nofollowLinks = false
    noreferrerLinks = false
    plainIDAnchors = true
    skipHTML = false
    smartDashes = true
    smartypants = true
    smartypantsQuotesNBSP = false
    taskLists = true
  [markup.goldmark]
    [markup.goldmark.extensions]
      definitionList = true
      footnote = true
      linkify = true
      strikethrough = true
      table = true
      taskList = true
      typographer = true
    [markup.goldmark.parser]
      attribute = true
      autoHeadingID = true
      autoHeadingIDType = "github"
    [markup.goldmark.renderer]
      hardWraps = false
      unsafe = false
      xhtml = false
  [markup.highlight]
    codeFences = true
    guessSyntax = false
    hl_Lines = ""
    lineNoStart = 1
    lineNos = false
    lineNumbersInTable = true
    noClasses = true
    style = "monokai"
    tabWidth = 4
  [markup.tableOfContents]
    endLevel = 3
    ordered = false
    startLevel = 2

[privacy]
  [privacy.disqus]
    disable = false
  [privacy.googleAnalytics]
    anonymizeIP = false
    disable = false
    respectDoNotTrack = false
    useSessionStorage = false
  [privacy.instagram]
    disable = false
    simple = false
  [privacy.twitter]
    disable = false
    enableDNT = false
    simple = false
  [privacy.vimeo]
    disable = false
    enableDNT = false
    simple = false
  [privacy.youtube]
    disable = false
    privacyEnhanced = false

[minify]
  disableCSS = false
  disableHTML = false
  disableJS = false
  disableJSON = false
  disableSVG = false
  disableXML = false
  minifyOutput = false
  [minify.tdewolff]
    [minify.tdewolff.css]
      keepCSS2 = true
      precision = 0
    [minify.tdewolff.html]
      keepComments = false
      keepConditionalComments = true
      keepDefaultAttrVals = true
      keepDocumentTags = true
      keepEndTags = true
      keepQuotes = false
      keepWhitespace = false
    [minify.tdewolff.js]
      keepVarNames = false
      noNullishOperator = false
      precision = 0
    [minify.tdewolff.json]
      keepNumbers = false
      precision = 0
    [minify.tdewolff.svg]
      keepComments = false
      precision = 0
    [minify.tdewolff.xml]
      keepWhitespace = false